﻿namespace Util
{
    public static class FormatarCampos
    {
        public static string FormatarString(this string palavra)
        {
            return (string.IsNullOrEmpty(palavra)) ? "" : palavra.Trim();
        }
        public static string CampoRequestOuCampoBanco(this string campoRequest, string campoBanco)
        {
            return (string.IsNullOrEmpty(campoRequest)) ? campoBanco : campoRequest;
        }
        public static int CampoRequestOuCampoBanco(this int campoRequest, int campoBanco)
        {
            return (campoRequest <= 0) ? campoBanco : campoRequest;
        }
        public static double CampoRequestOuCampoBanco(this double campoRequest, double campoBanco)
        {
            return (campoRequest <= 0) ? campoBanco : campoRequest;
        }
    }
}

﻿namespace DTO
{
    public class UserLoginDTO
    {
        public string Login { get; set; }
        public string Senha { get; set; }
    }
    public class UserDTO
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Role { get; set; }
    }
}

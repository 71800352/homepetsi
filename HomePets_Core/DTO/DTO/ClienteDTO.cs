﻿namespace DTO
{
    public class ClienteDTO
    {
        public string NomeCompleto { get; set; }
        public string Apelido { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public string Rua { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
    }

}

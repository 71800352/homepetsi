﻿using Data.Model;
using Data.Repositories.Interface;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System;
using System.Threading.Tasks;
using Util;

namespace Service.Service
{
    public class UserService : ControllerBase, IUserService
    {
        private readonly ITokenService ServiceToken;
        private readonly IUserRepository Repository;

        public UserService(ITokenService serviceToken, IUserRepository repository)
        {
            ServiceToken = serviceToken;
            Repository = repository;
        }

        public async Task<IActionResult> Login(UserLoginDTO user)
        {
            try
            {
                if (ValidRequest(user, out string resposta))
                {
                    var userExists = await Repository.Get(user.Login, user.Senha);
                    if (userExists == null)
                        return NotFound();

                    var token = ServiceToken.GenerateToken(userExists);
                    userExists.Senha = "";
                    return Ok(new
                    {
                        usuario = userExists,
                        token
                    });
                }
                else
                {
                    return BadRequest(resposta);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private static bool ValidRequest(UserLoginDTO user, out string resposta)
        {
            if (string.IsNullOrEmpty(user.Login))
            {
                resposta = "O campo Login é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(user.Senha))
            {
                resposta = "O campo Senha é obrigatório";
                return false;
            }
            resposta = "";
            return true;
        }

        public async Task<IActionResult> Cadastrar(UserDTO user)
        {
            try
            {
                if(ValidCreate(user,out string resposta))
                {
                    var loginExists = await Repository.GetByUsername(user.Login.FormatarString());
                    if(loginExists != null)
                    {
                        return BadRequest("Login informado já existe na base");
                    }
                    if(await Repository.Add(new UserModel()
                    {
                        Id = Repository.GetMaxId(),
                        Login = user.Login.FormatarString(),
                        Senha = user.Senha.FormatarString(),
                        Role = user.Role.FormatarString()
                    }))
                    {
                        return NoContent();
                    }

                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    return BadRequest(resposta);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private static bool ValidCreate(UserDTO user, out string resposta)
        {
            if (string.IsNullOrEmpty(user.Login))
            {
                resposta = "O campo Login é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(user.Senha))
            {
                resposta = "O campo Senha é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(user.Role))
            {
                resposta = "O campo Role é obrigatório";
                return false;
            }
            resposta = "";
            return true;
        }

        public async Task<IActionResult> Get(int? id)
        {
            try
            {
                if (id != null)
                {
                    var user = await Repository.Get((int)id);

                    if (user == null)
                        return NotFound();
                    return Ok(user);
                }
                else
                {
                    var users = await Repository.Get();

                    if (users == null || users?.Count == 0)
                        return NotFound();
                    return Ok(users);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var userExists = await Repository.Get(id);
                if (userExists == null)
                    return NotFound();

                if(await Repository.Delete(userExists))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Put(int id, UserDTO user)
        {
            try
            {
                var userExists = await Repository.Get(id);
                if (userExists == null)
                    return NotFound();

                if( await Repository.Update(new UserModel()
                {
                    Id = id,
                    Login = user.Login.CampoRequestOuCampoBanco(userExists.Login),
                    Senha = user.Senha.CampoRequestOuCampoBanco(userExists.Senha),
                    Role = user.Role.CampoRequestOuCampoBanco(userExists.Role)
                }))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}

﻿using Data.Model;
using Data.Repositories.Interface;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System;
using System.Threading.Tasks;
using Util;

namespace Service.Service
{
    public class ClienteService : ControllerBase, IClienteService
    {
        private readonly IClienteRepository Repository;
        private readonly IPetRepository PetRepository;

        public ClienteService(IClienteRepository repository, IPetRepository petRepository)
        {
            Repository = repository;
            PetRepository = petRepository;
        }

        public async Task<IActionResult> Post(ClienteDTO cliente)
        {
            try
            {
                if (ValidPost(cliente, out string resposta))
                {
                    if (await Repository.Add(new ClienteModel()
                    {
                        Id = Repository.GetMaxId(),
                        Apelido = cliente.Apelido.FormatarString(),
                        NomeCompleto = cliente.NomeCompleto.FormatarString(),
                        CPF = cliente.CPF.FormatarString(),
                        Telefone = cliente.Telefone.FormatarString(),
                        Rua = cliente.Rua,
                        Numero = cliente.Numero,
                        Bairro = cliente.Bairro.FormatarString(),
                        Cidade = cliente.Cidade.FormatarString(),
                        Estado = cliente.Estado.FormatarString()
                    }))
                    {
                        return NoContent();
                    }
                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    return BadRequest(resposta);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private static bool ValidPost(ClienteDTO cliente, out string resposta)
        {
            if (string.IsNullOrEmpty(cliente.NomeCompleto))
            {
                resposta = "Campo Nome do Cliente é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(cliente.Apelido))
            {
                resposta = "Campo Apelido do Cliente é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(cliente.CPF))
            {
                resposta = "Campo CPF do Cliente é obrigatório";
                return false;
            }
            resposta = "";
            return true;
        }

        public async Task<IActionResult> Get(int? id)
        {
            try
            {
                if (id != null)
                {
                    var cliente = await Repository.Get((int)id);

                    if (cliente == null)
                        return NotFound();
                    return Ok(cliente);
                }
                else
                {
                    var clientes = await Repository.Get();

                    if (clientes == null || clientes?.Count == 0)
                        return NotFound();
                    return Ok(clientes);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Put(int id, ClienteDTO cliente)
        {
            try
            {
                var clienteExiste = await Repository.Get((int)id);
                if (clienteExiste == null)
                    return NotFound();

                if (await Repository.Update(new ClienteModel()
                {
                    Id = id,
                    Apelido = cliente.Apelido.CampoRequestOuCampoBanco(clienteExiste.Apelido),
                    NomeCompleto = cliente.NomeCompleto.CampoRequestOuCampoBanco(clienteExiste.NomeCompleto),
                    CPF = cliente.CPF.CampoRequestOuCampoBanco(clienteExiste.CPF),
                    Telefone = cliente.Telefone.CampoRequestOuCampoBanco(clienteExiste.Telefone),
                    Rua = cliente.Rua.CampoRequestOuCampoBanco(clienteExiste.Rua),
                    Numero = cliente.Numero.CampoRequestOuCampoBanco(cliente.Numero),
                    Bairro = cliente.Bairro.CampoRequestOuCampoBanco(clienteExiste.Bairro),
                    Cidade = cliente.Cidade.CampoRequestOuCampoBanco(clienteExiste.Cidade),
                    Estado = cliente.Estado.CampoRequestOuCampoBanco(clienteExiste.Estado)
                }))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var clienteExiste = await Repository.Get(id);
                if (clienteExiste == null)
                    return NotFound();

                if (await Repository.Delete(clienteExiste))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> PostPet(PetModel pet)
        {
            try
            {
                if (await PetRepository.Add(new PetModel()
                {
                    Id = PetRepository.GetMaxId(),
                    NomeDoPet = pet.NomeDoPet.FormatarString(),
                    ClienteId = pet.ClienteId,
                    Idade = pet.Idade,
                    PesoDoPet = pet.PesoDoPet,
                    RacaDoPet = pet.RacaDoPet
                }))
                {
                    return NoContent();
                }
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> GetPet(int? id)
        {
            try
            {
                if (id != null)
                {
                    var pet = await PetRepository.Get((int)id);

                    if (pet == null)
                        return NotFound();
                    return Ok(pet);
                }
                else
                {
                    var pets = await PetRepository.Get();

                    if (pets == null || pets?.Count == 0)
                        return NotFound();
                    return Ok(pets);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> PutPet(int id, PetModel pet)
        {
            try
            {
                var petExiste = await PetRepository.Get((int)id);
                if (petExiste == null)
                    return NotFound();

                if (await PetRepository.Update(new PetModel()
                {
                    Id = id,
                    NomeDoPet = pet.NomeDoPet.CampoRequestOuCampoBanco(petExiste.NomeDoPet),
                    ClienteId = pet.ClienteId.CampoRequestOuCampoBanco(petExiste.ClienteId),
                    Idade = pet.Idade.CampoRequestOuCampoBanco(petExiste.Idade),
                    PesoDoPet = pet.PesoDoPet.CampoRequestOuCampoBanco(petExiste.PesoDoPet),
                    RacaDoPet = pet.RacaDoPet.CampoRequestOuCampoBanco(petExiste.RacaDoPet)
                }))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> DeletePet(int id)
        {
            try
            {
                var petExiste = await PetRepository.Get(id);
                if (petExiste == null)
                    return NotFound();

                if (await PetRepository.Delete(petExiste))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}

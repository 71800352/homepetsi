﻿using Data.Model;
using Data.Repositories.Interface;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System;
using System.Threading.Tasks;
using Util;

namespace Service.Service
{
    public class HotelService : ControllerBase, IHotelService
    {
        private readonly IHotelRepository Repository;
        private readonly IHoteDadosRepository HotelRepository;

        public HotelService(IHotelRepository repository, IHoteDadosRepository hotelRepository)
        {
            Repository = repository;
            HotelRepository = hotelRepository;
        }

        public async Task<IActionResult> Post(HotelDTO hotel)
        {
            try
            {
                if (ValidPost(hotel, out string resposta))
                {
                    var hotelExists = await Repository.FindByCNPJ(hotel.CNPJ.FormatarString());
                    if (hotelExists != null)
                        return BadRequest("CNPJ já existe na base");

                    if (await Repository.Add(new HotelModel()
                    {
                        Id = Repository.GetMaxId(),
                        CNPJ = hotel.CNPJ.FormatarString(),
                        NomeDoHotel = hotel.NomeDoHotel.FormatarString(),
                        Rua = hotel.Rua.FormatarString(),
                        Numero = hotel.Numero,
                        Bairro = hotel.Bairro.FormatarString(),
                        Cidade = hotel.Cidade.FormatarString(),
                        Estado = hotel.Estado.FormatarString(),
                        Telefone = hotel.Telefone.FormatarString()
                    }))
                    {
                        return NoContent();
                    }

                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    return BadRequest(resposta);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private static bool ValidPost(HotelDTO hotel, out string resposta)
        {
            if (string.IsNullOrEmpty(hotel.NomeDoHotel))
            {
                resposta = "Campo Nome do Hotel é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(hotel.CNPJ))
            {
                resposta = "Campo CNPJ do Hotel é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(hotel.Rua))
            {
                resposta = "Campo Rua do Hotel é obrigatório";
                return false;
            }
            if (string.IsNullOrEmpty(hotel.Bairro))
            {
                resposta = "Campo Bairro do Hotel é obrigatório";
                return false;
            }
            if (hotel.Numero <= 0)
            {
                resposta = "Campo Nome do Hotel é obrigatório";
                return false;
            }
            resposta = "";
            return true;
        }

        public async Task<IActionResult> Get(int? id)
        {
            try
            {
                if (id != null)
                {
                    var hotel = await Repository.Get((int)id);

                    if (hotel == null)
                        return NotFound();
                    return Ok(hotel);
                }
                else
                {
                    var hotels = await Repository.Get();

                    if (hotels == null || hotels?.Count == 0)
                        return NotFound();
                    return Ok(hotels);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Put(int id, HotelDTO hotel)
        {
            try
            {
                var hotelExists = await Repository.Get(id);

                if (hotelExists == null)
                    return NotFound();

                if(await Repository.Update(new HotelModel()
                {
                    Id = id,
                    CNPJ = hotel.CNPJ.CampoRequestOuCampoBanco(hotel.CNPJ),
                    NomeDoHotel = hotel.NomeDoHotel.CampoRequestOuCampoBanco(hotel.NomeDoHotel),
                    Rua = hotel.Rua.CampoRequestOuCampoBanco(hotel.Rua),
                    Numero = hotel.Numero.CampoRequestOuCampoBanco(hotel.Numero),
                    Bairro = hotel.Bairro.CampoRequestOuCampoBanco(hotel.Bairro),
                    Cidade = hotel.Cidade.CampoRequestOuCampoBanco(hotel.Cidade),
                    Estado = hotel.Estado.CampoRequestOuCampoBanco(hotel.Estado),
                    Telefone = hotel.Telefone.CampoRequestOuCampoBanco(hotel.Telefone)
                }))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var hotelExists = await Repository.Get(id);
                if (hotelExists == null)
                    return NotFound();

                if(await Repository.Delete(hotelExists))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> PostHotel(HotelDadosModel hotel)
        {
            try
            {
                if (await HotelRepository.Add(new HotelDadosModel()
                {
                    Id = HotelRepository.GetMaxId(),
                    Estrelas = hotel.Estrelas,
                    HotelId = hotel.HotelId,
                    PossuiBrinquedos = hotel.PossuiBrinquedos.FormatarString(),
                    ValorDiaria = hotel.ValorDiaria,
                    ValorMensal = hotel.ValorMensal,
                    ValorTrimestral = hotel.ValorTrimestral
                }))
                {
                    return NoContent();
                }
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> GetHotel(int? id)
        {
            try
            {
                if (id != null)
                {
                    var hotel = await HotelRepository.Get((int)id);

                    if (hotel == null)
                        return NotFound();
                    return Ok(hotel);
                }
                else
                {
                    var hotels = await HotelRepository.Get();

                    if (hotels == null || hotels?.Count == 0)
                        return NotFound();
                    return Ok(hotels);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> DeleteHotel(int id)
        {
            try
            {
                var petExiste = await HotelRepository.Get(id);
                if (petExiste == null)
                    return NotFound();

                if (await HotelRepository.Delete(petExiste))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public async Task<IActionResult> PutHotel(int id, HotelDadosModel hotel)
        {
            try
            {
                var petExiste = await HotelRepository.Get((int)id);
                if (petExiste == null)
                    return NotFound();

                if (await HotelRepository.Update(new HotelDadosModel()
                {
                    Id = id,
                    Estrelas = hotel.Estrelas.CampoRequestOuCampoBanco(petExiste.Estrelas),
                    HotelId = hotel.HotelId.CampoRequestOuCampoBanco(petExiste.Estrelas),
                    PossuiBrinquedos = hotel.PossuiBrinquedos.CampoRequestOuCampoBanco(petExiste.PossuiBrinquedos),
                    ValorDiaria = hotel.ValorDiaria.CampoRequestOuCampoBanco(petExiste.ValorDiaria),
                    ValorMensal = hotel.ValorMensal.CampoRequestOuCampoBanco(petExiste.ValorMensal),
                    ValorTrimestral = hotel.ValorMensal.CampoRequestOuCampoBanco(petExiste.ValorMensal)
                }))
                {
                    return NoContent();
                }

                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}

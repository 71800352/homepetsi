﻿using Data.Model;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IClienteService
    {
        Task<IActionResult> Post(ClienteDTO cliente);
        Task<IActionResult> Get(int? id);
        Task<IActionResult> Put(int id, ClienteDTO cliente);
        Task<IActionResult> Delete(int id);
        Task<IActionResult> PostPet(PetModel pet);
        Task<IActionResult> GetPet(int? id);
        Task<IActionResult> PutPet(int id, PetModel pet);
        Task<IActionResult> DeletePet(int id);
    }
}

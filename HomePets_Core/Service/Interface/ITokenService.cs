﻿using Data.Model;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface ITokenService
    {
        string GenerateToken(UserModel user);
    }
}

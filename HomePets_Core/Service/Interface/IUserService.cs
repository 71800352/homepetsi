﻿using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IUserService
    {
        Task<IActionResult> Login(UserLoginDTO user);
        Task<IActionResult> Cadastrar(UserDTO user);
        Task<IActionResult> Get(int? id);
        Task<IActionResult> Delete(int id);
        Task<IActionResult> Put(int id, UserDTO cliente);
    }
}

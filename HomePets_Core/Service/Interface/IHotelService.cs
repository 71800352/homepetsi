﻿using Data.Model;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IHotelService
    {
        Task<IActionResult> Post(HotelDTO cliente);
        Task<IActionResult> Get(int? id);
        Task<IActionResult> Put(int id, HotelDTO hotel);
        Task<IActionResult> Delete(int id);
        Task<IActionResult> PostHotel(HotelDadosModel hotel);
        Task<IActionResult> GetHotel(int? id);
        Task<IActionResult> DeleteHotel(int id);
        Task<IActionResult> PutHotel(int id, HotelDadosModel hotel);
    }
}

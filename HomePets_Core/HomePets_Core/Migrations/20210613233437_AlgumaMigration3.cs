﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HomePets_Core.Migrations
{
    public partial class AlgumaMigration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HotelDados",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HotelId = table.Column<int>(type: "INTEGER", nullable: false),
                    ValorDiaria = table.Column<double>(type: "REAL", nullable: false),
                    ValorMensal = table.Column<double>(type: "REAL", nullable: false),
                    ValorTrimestral = table.Column<double>(type: "REAL", nullable: false),
                    PossuiBrinquedos = table.Column<string>(type: "TEXT", nullable: true),
                    Estrelas = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HotelDados", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HotelDados");
        }
    }
}

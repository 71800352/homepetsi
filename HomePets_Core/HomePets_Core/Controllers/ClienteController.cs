﻿using Data.Model;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System.Threading.Tasks;

namespace HomePets_Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteService _service;

        public ClienteController(IClienteService service)
        {
            _service = service;
        }
        [HttpPost]
        public async Task<IActionResult> Post(ClienteDTO cliente)
        {
            return await _service.Post(cliente);
        }
        [HttpGet]
        public async Task<IActionResult> Get(int? id)
        {
            return await _service.Get(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id,ClienteDTO cliente)
        {
            return await _service.Put(id, cliente);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _service.Delete(id);
        }
        [HttpPost("/pets")]
        public async Task<IActionResult> PostPet(PetModel pet)
        {
            return await _service.PostPet(pet);
        }
        [HttpGet("/pets")]
        public async Task<IActionResult> GetPet(int? id)
        {
            return await _service.GetPet(id);
        }
        [HttpDelete("pets/{id}")]
        public async Task<IActionResult> PetDelete(int id)
        {
            return await _service.DeletePet(id);
        }
        [HttpPut("pets/{id}")]
        public async Task<IActionResult> PetPut(int id, PetModel pet)
        {
            return await _service.PutPet(id, pet);
        }
    }
}

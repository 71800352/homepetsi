﻿using DTO;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System.Threading.Tasks;

namespace HomePets_Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }
        [HttpPost]
        public async Task<IActionResult> Cadastrar(UserDTO user)
        {
            return await _service.Cadastrar(user);
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO user)
        {
            return await _service.Login(user);
        }
        [HttpGet]
        public async Task<IActionResult> Get(int? id)
        {
            return await _service.Get(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, UserDTO cliente)
        {
            return await _service.Put(id, cliente);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _service.Delete(id);
        }
    }
}

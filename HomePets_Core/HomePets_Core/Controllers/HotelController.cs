﻿using Data.Model;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System.Threading.Tasks;

namespace HomePets_Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly IHotelService _service;

        public HotelController(IHotelService service)
        {
            _service = service;
        }
        [HttpPost]
        public async Task<IActionResult> Post(HotelDTO hotel)
        {
            return await _service.Post(hotel);
        }
        [HttpGet]
        public async Task<IActionResult> Get(int? id)
        {
            return await _service.Get(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, HotelDTO hotel)
        {
            return await _service.Put(id, hotel);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await _service.Delete(id);
        }
        [HttpPost("/hotelDados")]
        public async Task<IActionResult> PostPet(HotelDadosModel hotel)
        {
            return await _service.PostHotel(hotel);
        }
        [HttpGet("/hotelDados")]
        public async Task<IActionResult> GetPet(int? id)
        {
            return await _service.GetHotel(id);
        }
        [HttpDelete("hotelDados/{id}")]
        public async Task<IActionResult> PetDelete(int id)
        {
            return await _service.DeleteHotel(id);
        }
        [HttpPut("hotelDados/{id}")]
        public async Task<IActionResult> PetPut(int id, HotelDadosModel hotel)
        {
            return await _service.PutHotel(id, hotel);
        }
    }
}

﻿namespace Data.Model
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Role { get; set; }
        public string CPFCNPJ { get; set; }
    }
}

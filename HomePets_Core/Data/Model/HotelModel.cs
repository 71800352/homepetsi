﻿namespace Data.Model
{
    public class HotelModel
    {
        public int Id { get; set; }
        public string CNPJ { get; set; }
        public string NomeDoHotel { get; set; }
        public string Telefone { get; set; }
        public string Rua { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
    }
}

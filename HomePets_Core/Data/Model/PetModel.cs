﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Model
{
    public class PetModel
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public string NomeDoPet { get; set; }
        public int PesoDoPet { get; set; }
        public string RacaDoPet { get; set; }
        public int Idade { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Model
{
    public class HotelDadosModel
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public double ValorDiaria { get; set; }
        public double ValorMensal { get; set; }
        public double ValorTrimestral { get; set; }
        public string PossuiBrinquedos { get; set; }
        public int Estrelas { get; set; }
    }
}

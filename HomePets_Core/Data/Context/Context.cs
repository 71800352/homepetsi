﻿using Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }
        public DbSet<ClienteModel> ClienteModel { get; set; }
        public DbSet<UserModel> UserModel { get; set; }
        public DbSet<HotelModel> HotelModel { get; set; }
        public DbSet<PetModel> PetModel { get; set; }
        public DbSet<HotelDadosModel> HotelDadosModel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlite(@"Data Source=HomePetsDB.db;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClienteModel>().ToTable("Clientes");
            modelBuilder.Entity<UserModel>().ToTable("Users");
            modelBuilder.Entity<HotelModel>().ToTable("Hotel");
            modelBuilder.Entity<PetModel>().ToTable("Pet");
            modelBuilder.Entity<HotelDadosModel>().ToTable("HotelDados");
            base.OnModelCreating(modelBuilder);
        }
    }
}

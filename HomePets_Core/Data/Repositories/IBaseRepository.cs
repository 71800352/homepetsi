﻿using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IBaseRepository
    {
        public Task<bool> Add<T>(T entity) where T : class;
        public Task<bool> Delete<T>(T entity) where T : class;
        public Task<bool> Update<T>(T entity) where T : class;
        int GetMaxId();
    }
}

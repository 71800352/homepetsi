﻿using Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface IHoteDadosRepository : IBaseRepository
    {
        Task<List<HotelDadosModel>> Get();
        Task<HotelDadosModel> Get(int id);
    }
}

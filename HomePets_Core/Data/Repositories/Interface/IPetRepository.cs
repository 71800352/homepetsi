﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface IPetRepository: IBaseRepository
    {
        Task<List<PetModel>> Get();
        Task<PetModel> Get(int id);
    }
}

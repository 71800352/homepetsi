﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface IUserRepository : IBaseRepository
    {
        Task<UserModel> Get(string login, string senha);
        Task<UserModel> GetByUsername(string login);
        Task<UserModel> Get(int id);
        Task<List<UserModel>> Get();
    }
}

﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface IClienteRepository : IBaseRepository
    {
        Task<List<ClienteModel>> Get();
        Task<ClienteModel> Get(int id);
    }
}

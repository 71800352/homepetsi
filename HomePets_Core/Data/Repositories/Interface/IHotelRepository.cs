﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories.Interface
{
    public interface IHotelRepository : IBaseRepository
    {
        Task<HotelModel> FindByCNPJ(string cnpj);
        Task<List<HotelModel>> Get();
        Task<HotelModel> Get(int id);
    }
}

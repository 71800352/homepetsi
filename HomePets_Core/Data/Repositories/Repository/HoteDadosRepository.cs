﻿using Data.Model;
using Data.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.Repository
{
    public class HoteDadosRepository : IHoteDadosRepository
    {
        public Context Context { get; set; }
        public HoteDadosRepository(Context context)
        {
            this.Context = context;
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public async Task<bool> Add<T>(T entity) where T : class
        {
            Context.Add(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            Context.Remove(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<List<HotelDadosModel>> Get()
        {
            IQueryable<HotelDadosModel> query = Context.HotelDadosModel;
            query = query.AsNoTracking();
            return await query.ToListAsync<HotelDadosModel>();
        }

        public async Task<HotelDadosModel> Get(int id)
        {
            IQueryable<HotelDadosModel> query = Context.HotelDadosModel;
            query = query.AsNoTracking()
                .Where(u => u.Id == id);
            return await query.FirstOrDefaultAsync();
        }

        public int GetMaxId()
        {
            IQueryable<HotelDadosModel> query = Context.HotelDadosModel;
            int MaxId = query.AsNoTracking()
                .AsEnumerable()
                .Select(u => u.Id).DefaultIfEmpty(0).Max();

            return MaxId + 1;
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            Context.Update(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }
    }
}

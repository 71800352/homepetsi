﻿using Data.Model;
using Data.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly Context Context;

        public UserRepository(Context context)
        {
            Context = context;
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<bool> Add<T>(T entity) where T : class
        {
            Context.Add(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            Context.Remove(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<UserModel> Get(string login, string senha)
        {
            IQueryable<UserModel> query = Context.UserModel;
            query = query.AsNoTracking()
                .Where(u => u.Login == login)
                .Where(u => u.Senha == senha);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            Context.Update(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public int GetMaxId()
        {
            IQueryable<UserModel> query = Context.UserModel;
            int MaxId = query.AsNoTracking()
                .AsEnumerable()
                .Select(u => u.Id).DefaultIfEmpty(0).Max();

            return MaxId + 1;
        }

        public async Task<UserModel> GetByUsername(string login)
        {
            IQueryable<UserModel> query = Context.UserModel;
            query = query.AsNoTracking()
                .Where(u => u.Login == login);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<UserModel> Get(int id)
        {
            IQueryable<UserModel> query = Context.UserModel;
            query = query.AsNoTracking()
                .Where(u => u.Id == id);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<List<UserModel>> Get()
        {
            IQueryable<UserModel> query = Context.UserModel;
            query = query.AsNoTracking();
            return await query.ToListAsync<UserModel>();
        }
    }
}

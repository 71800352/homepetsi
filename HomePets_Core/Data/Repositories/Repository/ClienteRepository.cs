﻿using Data.Repositories.Interface;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Data.Model;
using System.Collections.Generic;

namespace Data.Repositories.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        public Context Context { get; set; }

        public ClienteRepository(Context context)
        {
            this.Context = context;
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<bool> Add<T>(T entity) where T : class
        {
            Context.Add(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            Context.Remove(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await (Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            Context.Update(entity);
            return await (Context.SaveChangesAsync()) > 0;
        }

        public int GetMaxId()
        {
            IQueryable<ClienteModel> query = Context.ClienteModel;
            int MaxId = query.AsNoTracking()
                .AsEnumerable()
                .Select(u => u.Id).DefaultIfEmpty(0).Max();

            return MaxId + 1;
        }

        public async Task<List<ClienteModel>> Get()
        {
            IQueryable<ClienteModel> query = Context.ClienteModel;
            query = query.AsNoTracking();
            return await query.ToListAsync<ClienteModel>();
        }

        public async Task<ClienteModel> Get(int id)
        {
            IQueryable<ClienteModel> query = Context.ClienteModel;
            query = query.AsNoTracking()
                .Where(u => u.Id == id);
            return await query.FirstOrDefaultAsync();
        }
    }
}

﻿using Data.Model;
using Data.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Repository
{
    public class HotelRepository : IHotelRepository
    {
        public Context Context { get; set; }

        public HotelRepository(Context context)
        {
            Context = context;
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<bool> Add<T>(T entity) where T : class
        {
            Context.Add(entity);
            return await (Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            Context.Remove(entity);
            return await (Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            Context.Update(entity);
            return await (Context.SaveChangesAsync()) > 0;
        }

        public int GetMaxId()
        {
            IQueryable<HotelModel> query = Context.HotelModel;
            int MaxId = query.AsNoTracking()
                .AsEnumerable()
                .Select(u => u.Id).DefaultIfEmpty(0).Max();

            return MaxId + 1;
        }

        public async Task<HotelModel> FindByCNPJ(string cnpj)
        {
            IQueryable<HotelModel> query = Context.HotelModel;
            query = query.AsNoTracking()
                .Where(u => u.CNPJ == cnpj);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<List<HotelModel>> Get()
        {
            IQueryable<HotelModel> query = Context.HotelModel;
            query = query.AsNoTracking();
            return await query.ToListAsync<HotelModel>();
        }

        public async Task<HotelModel> Get(int id)
        {
            IQueryable<HotelModel> query = Context.HotelModel;
            query = query.AsNoTracking()
                .Where(u => u.Id == id);
            return await query.FirstOrDefaultAsync();
        }
    }
}

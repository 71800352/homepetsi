﻿using Data.Model;
using Data.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Repository
{
    public class PetRepository : IPetRepository
    {
        public Context Context { get; set; }
        public PetRepository(Context context)
        {
            this.Context = context;
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public async Task<bool> Add<T>(T entity) where T : class
        {
            Context.Add(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            Context.Remove(entity);
            return await(Context.SaveChangesAsync()) > 0;
        }

        public int GetMaxId()
        {
            IQueryable<PetModel> query = Context.PetModel;
            int MaxId = query.AsNoTracking()
                .AsEnumerable()
                .Select(u => u.Id).DefaultIfEmpty(0).Max();

            return MaxId + 1;
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            Context.Update(entity);
            return await  (Context.SaveChangesAsync()) > 0;
        }

        public async Task<List<PetModel>> Get()
        {
            IQueryable<PetModel> query = Context.PetModel;
            query = query.AsNoTracking();
            return await query.ToListAsync<PetModel>();
        }

        public async Task<PetModel> Get(int id)
        {
            IQueryable<PetModel> query = Context.PetModel;
            query = query.AsNoTracking()
                .Where(u => u.Id == id);
            return await query.FirstOrDefaultAsync();
        }
    }
}

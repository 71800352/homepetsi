﻿using DTO;
using Moq;
using Service.Interface;
using Service.Service;
using Test.ClassesMock;
using Test.RepositoriosMock;
using Xunit;

namespace Test.ServicesMock
{
    public class ClienteServiceMock
    {
        private IClienteService _service;

        [Fact]
        public void PostCliente_OK()
        {
            var clienteRepositorio = ClienteRepositoryMock.Instancia().PostClient_OK().Mock();
            //_service = new ClienteService(clienteRepositorio.Object);
            var retorno = _service.Post(ClienteMock.ClientePostMock());
            Assert.NotNull(retorno);
        }

    }
}

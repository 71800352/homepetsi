﻿using System.Threading.Tasks;
using Data.Model;
using Data.Repositories.Interface;
using Moq;

namespace Test.RepositoriosMock
{
    public class ClienteRepositoryMock
    {
        private readonly Mock<IClienteRepository> _mock = new();

        public static ClienteRepositoryMock Instancia()
        {
            return new ClienteRepositoryMock();
        }

        public Mock<IClienteRepository> Mock()
        {
            return _mock;
        }

        public ClienteRepositoryMock PostClient_OK()
        {
            _mock.Setup(repo => repo.Add(It.IsAny<ClienteModel>())).Returns(new Task<bool>(() => true));
            return this;
        }
    }
}

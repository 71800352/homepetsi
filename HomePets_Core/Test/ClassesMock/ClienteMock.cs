﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ClassesMock
{
    public class ClienteMock
    {
        public static ClienteDTO ClientePostMock()
        {
            return new ClienteDTO()
            {
                Apelido = "Teste",
                NomeCompleto = "Cliente de teste",
                CPF = "11211211639",
                Telefone = "33710041",
                Rua = "Rua Gama Cerqueira ",
                Numero = 1050,
                Bairro = "Jardim America",
                Cidade = "Belo Horizonte",
                Estado = "MG"
            };
        }
    }
}

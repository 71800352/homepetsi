import React from 'react';

const Modal =({props,onChange,onSubmitPet}) =>{
    console.log("MOOOOOOOOOOODAAAAAAAAAAAAAALLLLLLLLLLLLLL")
    return (
        <div className="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
                <div className="modal-header" backgroundColor="#e4605e" >
                    <h5 className="modal-title" id="TituloModalCentralizado">INCLUIR PET</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body bg-danger">
                    <div className="col-8">
                        <label for="inputAddress2" className="form-label">Nome do Pet</label>
                        <input type="text" name="nomePet" value={props.nomePet} className="form-control" onChange={onChange} id="inputAddress2" placeholder="Digite o nome completo do usuario" />
                    </div>
                    <div className="col-md-4">
                        <label for="inputCity" className="form-label ">Peso do Pet</label>
                        <input type="number" name="pesoPet" value={props.pesoPet} className="form-control" onChange={onChange} id="inputTel" />
                    </div>
                    <div className="col-md-4">
                        <label for="inputCity" className="form-label ">Raça do Pet</label>
                        <input type="text" name="racaPet" value={props.racaPet} className="form-control" onChange={onChange} id="inputTel" />
                    </div>
                    <div className="col-md-4">
                        <label for="inputCity" className="form-label ">Idade do Pet</label>
                        <input type="number" name="idade" value={props.idade} className="form-control" onChange={onChange} id="inputTel" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" className="btn btn-danger" onClick={onSubmitPet}>Incluir</button>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Modal
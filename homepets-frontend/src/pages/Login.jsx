import React, { useState } from 'react';
import './login.css';
import * as MdIcons from "react-icons/md";
import * as HiIcons from "react-icons/hi";
import * as FaIcons from "react-icons/fa";
import swal from "sweetalert";
import { Link } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';

function initialState() {
    return { user: '', password: '' };
}

const Login = () => {
    const [values, setValues] = useState(initialState);
    const [show, setShow] = useState(false);

    const handleClick = (event) => {
        event.preventDefault()
        setShow(!show);
    };

    const onChange = (event) => {
        const { value, name } = event.target;

        setValues({
            ...values,
            [name]: value
        });
    };

    const onSubmit = (event) => {
        event.preventDefault();
        const body = {
            login: values.user,
            senha: values.password,
        }
        axios.post("https://localhost:5001/api/User/login", body)
            .then((response) => {
                localStorage.setItem("USUARIO_ID",response.data.usuario.id);
                localStorage.setItem("ROLE",response.data.usuario.role);
                window.location.replace("http://localhost:3000/dashboard");
            })
            .catch(() => {
                toast.info("Usuário/senha inválidos");
            })
    };

    return (
        <div className='login'>
            <div className="login-logo">
                <img src=""
                    alt="Login App"
                />
            </div>
            <form onSubmit={onSubmit} >
                <div className="login-right">
                    <h1>LOGIN</h1>
                    <div className="login-loginInputEmail">
                        <FaIcons.FaUser />
                        <input
                            name="user"
                            type="text"
                            placeholder="Digite seu usuário"
                            value={values.user}
                            onChange={onChange} />
                    </div>
                    <div className="login-loginInputPassword">
                        <MdIcons.MdLock />
                        <input
                            name="password"
                            type={show ? "text" : "password"}
                            placeholder="Digite sua senha"
                            value={values.password}
                            onChange={onChange} />
                        <div className="login-eye">
                            {show ? (<HiIcons.HiEye
                                size={20}
                                onClick={handleClick} />
                            ) : (
                                <HiIcons.HiEyeOff
                                    size={20}
                                    onClick={handleClick} />
                            )}
                        </div>
                    </div>
                    <button type="submit" name="bntLogin">
                        ENTRAR
                </button>
                    <button type="button" data-toggle="modal" data-target="#exampleModal">
                        REGISTRAR
                </button>
                    <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-footer">
                                    <button type="button" ><a href='/cadastroCliente'>NOVO CLIENTE</a></button>
                                    <button type="button" ><a href='/cadastroHotel'>NOVO ESTABELECIMENTO</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Link to="/remember"><small >Esqueci minha senha!</small></Link>
                </div>
            </form>
        </div>
    );
};
export default Login;
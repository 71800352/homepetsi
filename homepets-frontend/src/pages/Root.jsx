import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import StoreProvider from "../components/Store/Provider";
import Login from './Login'
import CadastroUsuario from './CadastroCliente'
import CadastroHotel from './cadastroHotel'
import {ToastContainer} from 'react-toastify';
import Dashboard from './dashboard'
import CadastroDadosPet from './cadastroDadosPet';
import CadastroDadosHotel from './cadastroDadosHotel'

const PagesRoot = () => (

    <Router >
        <StoreProvider>
            <ToastContainer
                position="bottom-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover/>
            <Switch>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/cadastroCliente" component={CadastroUsuario}/>
                <Route exact path="/cadastroHotel" component={CadastroHotel}/>
                <Route exact path="/dashboard" component={Dashboard}/>
                <Route exact path="/cadastroDadosPet" component={CadastroDadosPet}/>
                <Route exact path="/cadastroDadosHotel" component={CadastroDadosHotel}/>
            </Switch>

        </StoreProvider>
    </Router>
)


export default PagesRoot;
import React, { useState} from 'react'
import './cadastro.css';
import axios from "axios";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const initialState = () => {
    return {
        user: '',
        password: '',
        confirmPassword: '',
        nomeDoHotel: '',
        email: '',
        telefone: '',
        logradouro: '',
        numero: 0,
        cidade: '',
        estado: '',
        bairro: '',
        cnpj: '',
        apelido: ''
    };
}

const CadastroHotel = () => {
    const [values, setValues] = useState(initialState);

    const cadastroHotel = () => {
        console.log('cadastrohotel')
        const body = {
            cnpj: values.cnpj,
            nomeDoHotel: values.nomeDoHotel,
            telefone: values.telefone,
            rua: values.logradouro,
            numero: values.numero,
            bairro: values.bairro,
            estado: values.estado,
            cidade: values.cidade,
        }
        axios.post("https://localhost:44396/api/Hotel", body)
            .then(() => {
                cadastroUsuarioHotel();
                return true;
            })
            .catch(() => {
                toast.error("erro ao cadastrar cliente, favor contatar o administrador do sistema!");
                return false;
            })
    }

    const cadastroUsuarioHotel = () => {
        const body = {
            login: values.apelido,
            senha: values.password,
            role: "funcionario"
        }
        axios.post("https://localhost:44396/api/User", body)
            .then(() => {
                window.location.replace("http://localhost:3000/login");
            })
            .catch(() => {
                toast.error("erro ao cadastrar usuario, favor contatar o administrador do sistema!");
                return false
            })
    }

    function onChange(e) {
        const { value, name } = e.target;

        setValues({
            ...values,
            [name]: value
        });
    };

    function onSubmit(event) {
        event.preventDefault();
        if (validarCampos()) {
            if (cadastroHotel()) {
                setValues(initialState);

            }
        }
    };

    const validarCampos = () => {
        if (values.nomeDoHotel === "" ||
            values.numero === 0 ||
            values.email === '' ||
            values.telefone === "" ||
            values.logradouro === "" ||
            values.cidade === "" ||
            values.estado === "" ||
            values.bairro === "" ||
            values.cnpj === "") {
            toast.info("Favor preencher todos os campos!")
            return false;
        }
    }

    return (
        <div className="center">
            <section className="section-listForm">
                <div class="jumbotron bg-danger text-white">
                    <h1 class="display-6">CADASTRO DE ESTABELECIMENTO</h1>
                    <p class="lead">Cadastro de novos estabelecimentos do sistema.</p>
                    <hr class="my-6" />
                </div>
                <div className="col-md-auto p-1">
                    <form className="row">
                        <div className="col-8">
                            <label for="inputAddress2" className="form-label">Nome do Hotel</label>
                            <input type="text" name="nomeDoHotel" value={values.nomeDoHotel} className="form-control" onChange={onChange} id="inputAddress2" placeholder="Digite o nome completo do usuario" />
                        </div>
                        <div className="col-4">
                            <label for="inputAddress2" className="form-label">Login</label>
                            <input type="text" name="apelido" value={values.apelido} className="form-control" onChange={onChange} id="inputApelido" placeholder="Digite um apelido" />
                        </div>
                        <div className="col-8">
                            <label for="inputAddress" className="form-label">Email</label>
                            <input type="text" name="email" value={values.email} className="form-control" onChange={onChange} id="inputAddress" placeholder="1234 Main St" />
                        </div>
                        <div className="col-md-4">
                            <label for="inputCity" className="form-label ">Telefone</label>
                            <input type="text" name="telefone" value={values.telefone} onkeypress="$(this).mask('000.000.000-00');" onChange={onChange} className="form-control" id="inputTel" />
                        </div>
                        <div className="col-4">
                            <label for="inputAddress2" className="form-label">Logradouro</label>
                            <input type="text" name="logradouro" value={values.logradouro} className="form-control" onChange={onChange} id="inputRua" placeholder="Rua, Avenida..." />
                        </div>
                        <div className="col-md-2">
                            <label for="inputCity" className="form-label ">Numero</label>
                            <input type="number" name="numero" value={values.numero} className="form-control" onChange={onChange} id="inputCity" />
                        </div>
                        <div className="col-md-2">
                            <label for="inputZip" className="form-label">Bairro</label>
                            <input type="text" name="bairro" value={values.bairro} className="form-control" onChange={onChange} id="inputBairro" />
                        </div>
                        <div className="col-md-2">
                            <label for="inputCity" className="form-label ">Cidade</label>
                            <input type="text" name="cidade" value={values.cidade} className="form-control" onChange={onChange} id="inputCity" />
                        </div>
                        <div className="col-md-2">
                            <label for="inputState" className="form-label">Estado</label>
                            <input type="text" name="estado" value={values.estado} className="form-control" onChange={onChange} id="inputState" />
                        </div>
                        <div className="col-md-4 ">
                            <label for="inputEmail4" className="form-label">Senha</label>
                            <input type="password" name="password" value={values.password} onChange={onChange} className="form-control" id="inputEmail4" />
                        </div>
                        <div className="col-md-4">
                            <label for="inputPassword4" className="form-label">Digite a senha novamente</label>
                            <input type="password" name="confirmPassword" value={values.confirmPassword} onChange={onChange} className="form-control" id="inputPassword4" />
                        </div>
                        <div className="col-md-4">
                            <label for="inputZip" className="form-label">CNPJ</label>
                            <input type="text" name="cnpj" value={values.cnpj} className="form-control" onChange={onChange} id="inputZip" />
                        </div>
                        <div className="col-12">
                            <div className="form-check form-label">
                            </div>
                        </div>
                        <div className="col-12">
                            <button type="submit" onClick={onSubmit} className="btn btn-danger">Cadastrar</button>
                            <button type="button" className="btn btn-secondary"><a href='/login' class="nounderline">Já tenho cadastro</a></button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    );
};
export default CadastroHotel
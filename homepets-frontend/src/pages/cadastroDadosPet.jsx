import React, { useEffect, useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from './modal';

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    red: {
        backgroundColor: '#e4605e'
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%'
    },
    cardContent: {
        flexGrow: 1
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

const initialState = () => {
    return {
        user: '',
        password: '',
        confirmPassword: '',
        nomeCompleto: '',
        apelido: '',
        email: '',
        numero: 0,
        telefone: '',
        logradouro: '',
        cidade: '',
        estado: '',
        bairro: '',
        cpf: '',
        nomePet: '',
        pesoPet: '',
        racaPet: '',
        idade: 0
    };

   
}

const CadastroDadosPet = () => {
    const classes = useStyles();
    const [ePut, setEPut] = useState(false);
    var [values, setValues] = useState(initialState);
    const [listaPet, setListaPets] = useState([])

    useEffect(()=>{
        axios.get(`https://localhost:5001/api/Cliente?id=1`)
            .then((response)=>{
                setValues({...values,
                    nomeCompleto:response.data.nomeCompleto,
                    email:"joao@cotemig.com.br",
                    telefone:response.data.telefone,
                    logradouro:response.data.rua,
                    numero:response.data.numero,
                    bairro:response.data.bairro,
                    cidade:response.data.cidade,
                    estado:response.data.estado
                })
            })
    },[])

    function onChange(e) {
        const { value, name } = e.target;

        setValues({
            ...values,
            [name]: value
        });

    };

    const onSubmit = (event) => {
        event.preventDefault();
    };

    const onSubmitPet = (event) => {
        event.preventDefault();
        const petsCopy = Array.from(listaPet)
        petsCopy.push(values)
        setListaPets(petsCopy);
        toast.success("Atualização realizada com sucesso ! ");
    };

    function deleteTask(index) {
        const itensCopy = Array.from(listaPet);
        itensCopy.splice(index, 1);
        setListaPets(itensCopy);
      }

      function updateTask({target}, index) {
        console.log(target,index)
        const itensCopy = Array.from(listaPet);
        
        //itensCopy.splice(index, 1, { id: index, value: target.value });
        setListaPets(itensCopy);

        return (
            <>
                <Modal props={index} onChange={onChange} onSubmitPet={onSubmitPet}
             ></Modal>
            </>
        )
      }

    useEffect(() => {
        const clienteId = localStorage.getItem('USUARIO_ID');
        axios.get(`https://localhost:5001/api/Cliente?id=${clienteId}`)
            .then((response) => {
                setValues({
                    ...values,
                    nomeCompleto: response.data.nomeCompleto,
                    email: "joao.cotemig@aluno.br",
                    logradouro: response.data.rua,
                    bairro: response.data.bairro,
                    numero: response.data.numero,
                    estado: response.data.estado,
                    cidade: response.data.cidade,
                    telefone: response.data.telefone
                });
            })

        axios.get(`https://localhost:5001/pets?id=${clienteId}`)
            .then((response) => {
                setEPut(true);
                setValues({
                    ...values,
                    nomePet: response.data.nomeDoPet,
                    pesoPet: response.data.pesoDoPet,
                    racaPet: response.data.racaDoPet,
                    idade: response.data.idade
                })
            })
            .catch(() => {
                setEPut(false);
            })
    }, [])

    return (
        <>
            <AppBar position="relative">
                <Toolbar className={classes.red}>
                    <CameraIcon className={classes.icon} />
                    <Typography variant="h6" color="inherit" noWrap>
                        Home Pets
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className="center">
                <section className="section-listForm">
                    <div class="jumbotron bg-danger text-white">
                        <h1 class="display-6">Perfil</h1>
                        <p class="lead">Dados Cadastrais</p>
                        <hr class="my-6" />
                    </div>
                    <div className="col-md-auto p-1">
                        <form className="row">
                            <div className="col-8">
                                <label for="inputAddress2" className="form-label">Nome Completo</label>
                                <input type="text" name="nomeCompleto" value={values.nomeCompleto} className="form-control" onChange={onChange} id="inputAddress2" placeholder="Digite o nome completo do usuario" />
                            </div>
                            <div className="col-8">
                                <label for="inputAddress" className="form-label">Email</label>
                                <input type="text" name="email" value={values.email} className="form-control" onChange={onChange} id="inputAddress" placeholder="exemplo@exemplo.com" />
                            </div>
                            <div className="col-md-4">
                                <label for="inputCity" className="form-label ">Telefone</label>
                                <input type="text" name="telefone" value={values.telefone} onkeypress="$(this).mask('000.000.000-00');" className="form-control" onChange={onChange} id="inputTel" />
                            </div>
                            <div className="col-4">
                                <label for="inputAddress2" className="form-label">Logradouro</label>
                                <input type="text" name="logradouro" value={values.logradouro} className="form-control" onChange={onChange} id="inputRua" placeholder="Rua, Avenida" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Numero</label>
                                <input type="number" name="numero" value={values.numero} className="form-control" onChange={onChange} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputZip" className="form-label">Bairro</label>
                                <input type="text" name="bairro" value={values.bairro} className="form-control" onChange={onChange} id="inputBairro" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Cidade</label>
                                <input type="text" name="cidade" value={values.cidade} className="form-control" onChange={onChange} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputState" className="form-label">Estado</label>
                                <input type="text" name="estado" value={values.estado} className="form-control" onChange={onChange} id="inputState" />
                            </div>
                            <div className="col-12">
                                <div className="form-check form-label">
                                    <label for="inputState" className="form-label">PETS CADASTRADOS:</label>
                                </div>
                            </div>
                            <div class="list-group p-1">
                                {listaPet.length > 0 &&
                                     listaPet.map((item, index) => {
                                        return (<Link 
                                                key={index}
                                                params={{item}}
                                                className="list-group-item list-group-item-action list-group-item-light flex-column align-items-start">
                                                   <div className="d-flex w-100 justify-content-between">
                                                   <h5 className="">{item.nomePet} </h5>
                                                     </div>
                                                     
                                                   <small className="p-1">Raça: {item.racaPet} --</small>
                                                     <small className = "p1">Idade: {item.idade}</small>
                                                     <div className="form-check form-label"></div>
                                                     <button type="button" className="btn btn-danger p-1" onClick={(event) =>  deleteTask(event,index) }>Excluir</button>
                                                      <button type="button" className="btn btn-primary p-1" onClick={(event) =>  updateTask(event,item) }>Editar</button>
                                          </Link>)
                                                 })}
                            </div>
                            <div className="col-12 p-2">
                                <button type="button" className="btn btn-danger p-1" data-toggle="modal" data-target="#ExemploModalCentralizado">
                                    Adicionar Pet
                                </button>
                                <div className="form-check form-label"><br /></div>
                                <div className="col-12">
                                    <button type="submit" onClick={onSubmit} className="btn btn-danger">Salvar</button>
                                </div>
                            </div>
                         
                          

                            <div className="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
                                <div className="modal-dialog modal-dialog-centered" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header" backgroundColor="#e4605e" >
                                            <h5 className="modal-title" id="TituloModalCentralizado">INCLUIR PET</h5>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Fechar">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div className="modal-body bg-danger">
                                            <div className="col-8">
                                                <label for="inputAddress2" className="form-label">Nome do Pet</label>
                                                <input type="text" name="nomePet" value={values.nomePet} className="form-control" onChange={onChange} id="inputAddress2" placeholder="Digite o nome completo do usuario" />
                                            </div>
                                            <div className="col-md-4">
                                                <label for="inputCity" className="form-label ">Peso do Pet</label>
                                                <input type="number" name="pesoPet" value={values.pesoPet} className="form-control" onChange={onChange} id="inputTel" />
                                            </div>
                                            <div className="col-md-4">
                                                <label for="inputCity" className="form-label ">Raça do Pet</label>
                                                <input type="text" name="racaPet" value={values.racaPet} className="form-control" onChange={onChange} id="inputTel" />
                                            </div>
                                            <div className="col-md-4">
                                                <label for="inputCity" className="form-label ">Idade do Pet</label>
                                                <input type="number" name="idade" value={values.idade} className="form-control" onChange={onChange} id="inputTel" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                            <button type="button" className="btn btn-danger" onClick={onSubmitPet}>Incluir</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </>
    )
}

export default CadastroDadosPet;
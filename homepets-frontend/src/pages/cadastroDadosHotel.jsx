import React, { useEffect, useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    red: {
        backgroundColor: '#e4605e'
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%'
    },
    cardContent: {
        flexGrow: 1
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

const initialState = () => {
    return {
        user: '',
        password: '',
        confirmPassword: '',
        nomeDoHotel: '',
        email: '',
        telefone: '',
        logradouro: '',
        numero: 0,
        cidade: '',
        estado: '',
        bairro: '',
        cnpj: '',
        apelido: '',
    };
}

const initialState1 = () => {
    return {
        diaria: '',
        vet: '',
        mensal: '',
        trimestal: '',
        brinquedos: '',
        estrelas: ''
    };
}

const CadastroDadosHotel = () => {
    const classes = useStyles();
    const [values, setValues] = useState(initialState);
    const [values1, setValues1] = useState(initialState1);
    const [ePut, setEPut] = useState(false);
    const [clienteID, setClienteID] = useState("");

    useEffect(() => {
        console.log("useEffect")
        const clienteId = localStorage.getItem('USUARIO_ID');
        setClienteID(clienteId);
        axios.get(`https://localhost:5001/api/Hotel?id=${1}`)
            .then((response) => {
                setValues({
                    ...values,
                    nomeDoHotel: response.data.nomeDoHotel,
                    email: "hotel.cotemig@aluno.br",
                    logradouro: response.data.rua,
                    bairro: response.data.bairro,
                    numero: response.data.numero,
                    estado: response.data.estado,
                    cidade: response.data.cidade,
                    telefone: response.data.telefone
                });
            })

        axios.get(`https://localhost:5001/hotelDados?id=${1}`)
            .then((response) => {
                setEPut(true);
                setValues1({
                    ...values1,
                    brinquedos: response.data.possuiBrinquedos,
                    estrelas: response.data.estrelas,
                    mensal: response.data.valorMensal,
                    trimestal: response.data.valorTrimestral,
                    diaria: response.data.valorDiaria,
                    vet: 'Sim'
                })
            })
            .catch(() => {
                setEPut(false);
            })
    }, [])


    function onChange(e) {
        const { value, name } = e.target;

        setValues({
            ...values,
            [name]: value
        });
    };

    function onChange1(e) {
        const { value, name } = e.target;

        setValues1({
            ...values1,
            [name]: value
        });
    };

    function onSubmit(event) {
        event.preventDefault();

        if (ePut) {
            const body = {
                cnpj: values.cnpj,
                nomeDoHotel: values.nomeDoHotel,
                telefone: values.telefone,
                rua: values.logradouro,
                numero: values.numero,
                bairro: values.bairro,
                estado: values.estado,
                cidade: values.cidade
            };

            axios.put(`https://localhost:5001/api/Hotel/${clienteID}`, body)
                .then(() => { });

            const body2 = {
                hotelId: clienteID,
                valorDiaria: values1.diaria,
                valorMensal: values1.mensal,
                valorTrimestral: values1.trimestal,
                possuiBrinquedos: values1.brinquedos,
                estrelas: values1.entrelas
            }
            console.log(body2)
            axios.put(`https://localhost:5001/api/Hotel/hotelDados/${1}`, body2)
                .then(() => {
                    toast.success("Alterações realizadas com sucesso !");
                });

        } else {
            const body2 = {
                hotelId: clienteID,
                valorDiaria: values1.diaria,
                valorMensal: values1.mensal,
                valorTrimestral: values1.trimestal,
                possuiBrinquedos: values1.brinquedos,
                estrelas: values1.entrelas
            };
            axios.put(`https://localhost:5001/hotelDados?id=${clienteID}`, body2)
                .then(() => {
                    toast.sucess("Alterações realizadas com sucesso !");
                });
        }
    };

    return (
        <>
            <AppBar position="relative">
                <Toolbar className={classes.red}>
                    <CameraIcon className={classes.icon} />
                    <Typography variant="h6" color="inherit" noWrap>
                        Home Pets
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className="center">
                <section className="section-listForm">
                    <div class="jumbotron bg-danger text-white">
                        <h1 class="display-6">CADASTRO DE ESTABELECIMENTO</h1>
                        <p class="lead">Cadastro de novos estabelecimentos do sistema.</p>
                        <hr class="my-6" />
                    </div>
                    <div className="col-md-auto p-1">
                        <form className="row">
                            <div className="col-8">
                                <label for="inputAddress2" className="form-label">Nome do Hotel</label>
                                <input type="text" name="nomeDoHotel" value={values.nomeDoHotel} className="form-control" onChange={onChange} id="inputAddress2" placeholder="Digite o nome completo do usuario" />
                            </div>
                            <div className="col-8">
                                <label for="inputAddress" className="form-label">Email</label>
                                <input type="text" name="email" value={values.email} className="form-control" onChange={onChange} id="inputAddress" placeholder="1234 Main St" />
                            </div>
                            <div className="col-md-4">
                                <label for="inputCity" className="form-label ">Telefone</label>
                                <input type="text" name="telefone" value={values.telefone} onkeypress="$(this).mask('000.000.000-00');" onChange={onChange} className="form-control" id="inputTel" />
                            </div>
                            <div className="col-4">
                                <label for="inputAddress2" className="form-label">Logradouro</label>
                                <input type="text" name="logradouro" value={values.logradouro} className="form-control" onChange={onChange} id="inputRua" placeholder="Rua, Avenida..." />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Numero</label>
                                <input type="number" name="numero" value={values.numero} className="form-control" onChange={onChange} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputZip" className="form-label">Bairro</label>
                                <input type="text" name="bairro" value={values.bairro} className="form-control" onChange={onChange} id="inputBairro" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Cidade</label>
                                <input type="text" name="cidade" value={values.cidade} className="form-control" onChange={onChange} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputState" className="form-label">Estado</label>
                                <input type="text" name="estado" value={values.estado} className="form-control" onChange={onChange} id="inputState" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Valor Diária</label>
                                <input type="text" name="diaria" value={values1.diaria} className="form-control" onChange={onChange} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Valor Mensal</label>
                                <input type="number" name="mensal" value={values1.mensal} className="form-control" onChange={onChange1} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Valor Trimestral</label>
                                <input type="number" name="trimestal" value={values1.trimestal} className="form-control" onChange={onChange1} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Possui Veterinário</label>
                                <input type="text" name="vet" value={values1.vet} className="form-control" onChange={onChange1} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Possui Brinquedos</label>
                                <input type="text" name="brinquedos" value={values1.brinquedos} className="form-control" onChange={onChange1} id="inputCity" />
                            </div>
                            <div className="col-md-2">
                                <label for="inputCity" className="form-label ">Estrelas</label>
                                <input type="text" name="estrelas" value={values1.estrelas} className="form-control" onChange={onChange1} id="inputCity" />
                            </div>
                            <div className="col-12">
                                <button type="submit" onClick={onSubmit} className="btn btn-danger">Atualizar</button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </>
    )
}

export default CadastroDadosHotel;
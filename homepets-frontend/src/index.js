import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'react-toastify/dist/ReactToastify.css';
import PagesRoot from './pages/Root';

ReactDOM.render(
    <PagesRoot/>
    , document.getElementById('root')
);


